
// Assignment 3 - Pointers
// <Your Name>


#include <iostream>
#include <conio.h>

using namespace std;

// TODO: Implement the "SwapIntegers" function
void SwapIntegers(int *first, int *second)
{
	int x;   // Create a new variable

	x = *first;  // Set the new variable to the value that *first points to (first user input)

	*first = *second; // Set the value the *first pointer points at (first user input)
	// to the value the the *second pointer points to (second user input)

	*second = x; // Set the value that the *second pointer points to (second user input) to the value
	// of the 'new' variable (previously set to first user input)

}

// Do not modify the main function!
int main()
{
	int first = 0;
	int second = 0;

	cout << "Enter the first integer: ";
	cin >> first;

	cout << "Enter the second integer: ";
	cin >> second;

	cout << "\nYou entered:\n";
	cout << "first: " << first << "\n";
	cout << "second: " << second << "\n";

	SwapIntegers(&first, &second);

	cout << "\nAfter swapping:\n";
	cout << "first: " << first << "\n";
	cout << "second: " << second << "\n";

	cout << "\nPress any key to quit.";

	_getch();
	return 0;
}
